<?php

declare(strict_types=1);

namespace App\ValueObject;

enum PageStatus: string
{
    case PUBLISHED = 'published';
    case DRAFT = 'draft';
    case WAITING_REVIEW = 'waiting_review';
    case ARCHIVED = 'archived';
}
