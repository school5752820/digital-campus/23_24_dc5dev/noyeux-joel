<?php

declare(strict_types=1);

namespace App\ValueObject\Response;

use App\Entity\User;
use Symfony\Component\Uid\Uuid;

class ShowUserResponse
{
    public Uuid $uuid;

    public string $email;

    public function __construct(User $user)
    {
        $this->uuid = $user->getUuid();
        $this->email = $user->getEmail();
    }

    public static function fromUser(User $user): static
    {
        return new static($user);
    }
}