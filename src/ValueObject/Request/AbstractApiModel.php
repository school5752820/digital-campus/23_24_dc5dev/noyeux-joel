<?php

declare(strict_types=1);

namespace App\ValueObject\Request;

use App\Exception\ApiValidationException;
use App\Service\JsonTransformer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class AbstractApiModel
{
    public function toJSON(): string
    {
        return JsonTransformer::serializer()->serialize($this, 'json');
    }

    public function validate(ValidatorInterface $validator): static
    {
        $validationErrors = $validator->validate($this);

        if (count($validationErrors) > 0) {
            throw new ApiValidationException($validationErrors);
        }

        return $this;
    }
}