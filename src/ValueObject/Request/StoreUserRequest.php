<?php

declare(strict_types=1);

namespace App\ValueObject\Request;

use Symfony\Component\Serializer\Attribute\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

class StoreUserRequest extends AbstractApiModel
{
    #[SerializedName('email')]
    #[Assert\NotBlank]
    #[Assert\Email]
    private string $email;

    #[SerializedName('password')]
    #[Assert\NotBlank]
    #[Assert\NotCompromisedPassword]
    private string $password;

    #[SerializedName('username')]
    #[Assert\NotBlank]
    private string $username;

    #[SerializedName('firstname')]
    #[Assert\NotBlank]
    private string $firstname;

    #[SerializedName('lastname')]
    #[Assert\NotBlank]
    private string $lastname;

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;
        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): static
    {
        $this->password = $password;
        return $this;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): static
    {
        $this->username = $username;
        return $this;
    }

    public function getFirstname(): string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): static
    {
        $this->firstname = $firstname;
        return $this;
    }

    public function getLastname(): string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): static
    {
        $this->lastname = $lastname;
        return $this;
    }
}