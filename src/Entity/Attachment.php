<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\AttachmentRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\HasLifecycleCallbacks]
#[ORM\Entity(repositoryClass: AttachmentRepository::class)]
#[ORM\Table('`attachment`')]
class Attachment extends AbstractEntity
{
    #[ORM\Column(type: Types::STRING, nullable: false)]
    private string $alt;

    #[ORM\Column(type: Types::STRING, nullable: false)]
    private string $filename;

    #[ORM\Column(type: Types::STRING, nullable: false)]
    private string $filepath;

    #[ORM\Column(type: Types::INTEGER, nullable: false)]
    private int $filesize;

    #[ORM\Column(type: Types::STRING, nullable: false)]
    private string $mimeType;

    public function getAlt(): string
    {
        return $this->alt;
    }

    public function setAlt(string $alt): Attachment
    {
        $this->alt = $alt;
        return $this;
    }

    public function getFilename(): string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): Attachment
    {
        $this->filename = $filename;
        return $this;
    }

    public function getFilepath(): string
    {
        return $this->filepath;
    }

    public function setFilepath(string $filepath): Attachment
    {
        $this->filepath = $filepath;
        return $this;
    }

    public function getFilesize(): int
    {
        return $this->filesize;
    }

    public function setFilesize(int $filesize): Attachment
    {
        $this->filesize = $filesize;
        return $this;
    }

    public function getMimeType(): string
    {
        return $this->mimeType;
    }

    public function setMimeType(string $mimeType): Attachment
    {
        $this->mimeType = $mimeType;
        return $this;
    }
}