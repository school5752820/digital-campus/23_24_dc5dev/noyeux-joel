<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\PageRepository;
use App\ValueObject\PageStatus;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\HasLifecycleCallbacks]
#[ORM\Entity(repositoryClass: PageRepository::class)]
#[ORM\Table(name: '`page`')]
class Page extends AbstractEntity
{
    #[ORM\Column(type: Types::STRING, nullable: false)]
    private string $title;

    #[ORM\Column(type: Types::STRING, nullable: false)]
    private string $description;

    #[ORM\Column(type: Types::STRING, unique: true, nullable: false)]
    private string $slug;

    #[ORM\ManyToOne(targetEntity: Attachment::class)]
    #[ORM\JoinColumn(nullable: false)]
    private Attachment $thumbnail;

    #[ORM\Column(type: Types::STRING, nullable: false)]
    private string $markdown;

    #[ORM\Column(type: Types::STRING, nullable: false)]
    private string $html;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE, nullable: false)]
    private \DateTimeImmutable $publishedAt;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: false)]
    private User $author;

    #[ORM\Column(type: Types::STRING, nullable: false)]
    private string $status;

    #[ORM\ManyToOne(targetEntity: Category::class, inversedBy: 'pages')]
    #[ORM\JoinColumn(nullable: false)]
    private Category $category;

    /** @var ArrayCollection<int, Tag> */
    #[ORM\ManyToMany(targetEntity: Tag::class, inversedBy: 'pages')]
    private Collection $tags;

    #[ORM\PrePersist]
    #[ORM\PreUpdate]
    public function generateSlug(): void
    {
        $this->slug = $this->_generateSlug($this->title);
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;
        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;
        return $this;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): static
    {
        $this->slug = $slug;
        return $this;
    }

    public function getThumbnail(): Attachment
    {
        return $this->thumbnail;
    }

    public function setThumbnail(Attachment $thumbnail): static
    {
        $this->thumbnail = $thumbnail;
        return $this;
    }

    public function getMarkdown(): string
    {
        return $this->markdown;
    }

    public function setMarkdown(string $markdown): static
    {
        $this->markdown = $markdown;
        return $this;
    }

    public function getHtml(): string
    {
        return $this->html;
    }

    public function setHtml(string $html): static
    {
        $this->html = $html;
        return $this;
    }

    public function getPublishedAt(): \DateTimeImmutable
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(\DateTimeImmutable $publishedAt): static
    {
        $this->publishedAt = $publishedAt;
        return $this;
    }

    public function getAuthor(): User
    {
        return $this->author;
    }

    public function setAuthor(User $author): static
    {
        $this->author = $author;
        return $this;
    }

    public function getStatus(): PageStatus
    {
        return PageStatus::from($this->status);
    }

    public function setStatus(PageStatus $status): static
    {
        $this->status = $status->value;
        return $this;
    }

    public function getCategory(): Category
    {
        return $this->category;
    }

    public function setCategory(Category $category): static
    {
        $this->category = $category;

        return $this;
    }

    /** @return ArrayCollection<int, Tag> */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): static
    {
        if (!$this->tags->contains($tag)) {
            $this->tags->add($tag);
        }

        return $this;
    }

    public function removeTag(Tag $tag): static
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
        }

        return $this;
    }
}