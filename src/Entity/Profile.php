<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\ProfileRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\HasLifecycleCallbacks]
#[ORM\Entity(repositoryClass: ProfileRepository::class)]
#[ORM\Table(name: '`profile`')]
#[UniqueEntity('username')]
class Profile extends AbstractEntity
{
    #[ORM\Column(type: Types::STRING, unique: true, nullable: false)]
    private string $username;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $birthday;

    #[ORM\Column(type: Types::STRING, nullable: false)]
    private string $firstname;

    #[ORM\Column(type: Types::STRING, nullable: false)]
    private string $lastname;

    #[ORM\OneToOne(mappedBy: 'profile', targetEntity: User::class)]
    private User $user;

    /** @var ArrayCollection<int, static> */
    #[ORM\ManyToMany(targetEntity: Profile::class, mappedBy: 'followed')]
    private Collection $followers;

    /** @var ArrayCollection<int, static> */
    #[ORM\ManyToMany(targetEntity: Profile::class, inversedBy: 'followers')]
    private Collection $followed;

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): static
    {
        $this->username = $username;

        return $this;
    }

    public function getBirthday(): ?\DateTimeImmutable
    {
        return $this->birthday;
    }

    public function setBirthday(?\DateTimeImmutable $birthday): static
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getFirstname(): string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): static
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): static
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): static
    {
        $this->user = $user;

        return $this;
    }

    /** @return ArrayCollection<int, Profile> */
    public function getFollowers(): Collection
    {
        return $this->followers;
    }

    /** @return ArrayCollection<int, Profile> */
    public function getFollowed(): Collection
    {
        return $this->followed;
    }

    public function addFollowed(Profile $profile): static
    {
        if (!$this->followed->contains($profile)) {
            $this->followed->add($profile);
        }

        return $this;
    }

    public function removeFollowed(Profile $profile): static
    {
        if ($this->followed->contains($profile)) {
            $this->followed->removeElement($profile);
        }

        return $this;
    }
}