<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ApiExceptionSubscriber implements EventSubscriberInterface
{
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        if ($exception instanceof AccessDeniedException) {
            $event->setResponse(
                new JsonResponse([
                    'status' => Response::HTTP_FORBIDDEN,
                    'payload' => [],
                    'errors' => [
                        "Vous n'avez pas les autorisations pour accéder à cette ressource",
                    ],
                ], Response::HTTP_FORBIDDEN)
            );
        } else if ($exception instanceof NotFoundHttpException) {
            $event->setResponse(
                new JsonResponse([
                    'status' => Response::HTTP_NOT_FOUND,
                    'payload' => [],
                    'errors' => [
                        "Aucune ressource trouvée",
                    ],
                ], Response::HTTP_NOT_FOUND)
            );
        } elseif ($exception instanceof UnauthorizedHttpException) {
            $event->setResponse(
                new JsonResponse([
                    'status' => Response::HTTP_UNAUTHORIZED,
                    'payload' => [],
                    'errors' => [
                        "Vous n'avez les accès à cette ressource",
                    ],
                ], Response::HTTP_UNAUTHORIZED)
            );
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => ['onKernelException', 2],
        ];
    }
}
