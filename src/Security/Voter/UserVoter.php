<?php

namespace App\Security\Voter;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class UserVoter extends Voter
{
    public const INDEX = 'user_index';
    public const STORE = 'user_store';
    public const SHOW = 'user_show';
    public const DELETE = 'user_delete';

    protected function supports(string $attribute, mixed $subject): bool
    {
        return (
            in_array($attribute, [self::INDEX, self::STORE, self::SHOW, self::DELETE])
            && $subject instanceof User
        );
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        /** @var User $user */
        $user = $subject;

        /** @var User|null $authenticatedUser */
        $authenticatedUser = $token->getUser();

        return match ($attribute) {
            self::INDEX, self::SHOW => true,
            self::STORE => $this->isAuthenticated($authenticatedUser) && $authenticatedUser->hasRole('ADMIN'),
            self::DELETE => (
                $this->isAuthenticated($authenticatedUser)
                && $authenticatedUser->hasRole('ADMIN')
                && $user->getUuid() !== $authenticatedUser->getUuid()
            )
        };
    }

    private function isAuthenticated(?UserInterface $user): bool
    {
        return $user instanceof UserInterface;
    }
}
