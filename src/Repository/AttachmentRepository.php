<?php

namespace App\Repository;

use App\Entity\Attachment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

/**
 * @extends ServiceEntityRepository<Attachment>
 *
 * @implements PasswordUpgraderInterface<Attachment>
 *
 * @method Attachment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Attachment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Attachment[]    findAll()
 * @method Attachment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AttachmentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Attachment::class);
    }

    public function save(Attachment $entity, bool $flush = false): void
    {
        $this->_em->persist($entity);
        $flush && $this->_em->flush();
    }

    public function remove(Attachment $entity, bool $flush = false): void
    {
        $this->_em->remove($entity);
        $flush && $this->_em->flush();
    }
}
