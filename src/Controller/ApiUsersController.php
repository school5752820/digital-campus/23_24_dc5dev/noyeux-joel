<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Profile;
use App\Entity\User;
use App\Exception\ApiValidationException;
use App\Repository\ProfileRepository;
use App\Repository\UserRepository;
use App\Service\JsonTransformer;
use App\ValueObject\Request\StoreUserRequest;
use App\ValueObject\Response\ShowUserResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route(path: '/api/users', name: 'api_users')]
class ApiUsersController extends AbstractApiController
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly ProfileRepository $profileRepository,
        private readonly ValidatorInterface $validator,
    ) {
    }

    #[Route(path: '', name: '_index', methods: ['GET'])]
    #[IsGranted('user_index', null)]
    public function index(#[CurrentUser] ?User $currentUser): Response
    {
        $users = $this->userRepository->findAll();
        $users = array_map(fn (User $user) => ShowUserResponse::fromUser($user), $users);

        return $this->returnJson(
            status: Response::HTTP_OK,
            payload: $users,
        );
    }

    #[Route(path: '', name: '_store', methods: ['POST'])]
    #[IsGranted('user_store', null)]
    public function store(#[CurrentUser] ?User $currentUser, Request $request, UserPasswordHasherInterface $hasher): Response
    {
        /** @var StoreUserRequest|null $requestPayload */
        $requestPayload = JsonTransformer::fromJSON($request->getContent(), StoreUserRequest::class);

        if (null === $requestPayload) {
            return $this->returnJson(
                status: Response::HTTP_UNPROCESSABLE_ENTITY,
                errors: [
                    "Impossible de désérialiser le contenu de la requête"
                ],
            );
        }

        try {
            /** @var StoreUserRequest $payload */
            $payload = $requestPayload->validate($this->validator);
        } catch (ApiValidationException $exception) {
            return $this->returnJson(
                status: Response::HTTP_UNPROCESSABLE_ENTITY,
                errors: $exception->getDisplayableViolations()
            );
        }

        $user = new User();
        $user
            ->setEmail($payload->getEmail())
            ->setPassword($hasher->hashPassword($user, $payload->getPassword()));

        $profile = (new Profile())
            ->setFirstname($payload->getFirstname())
            ->setLastname($payload->getLastname())
            ->setUsername($payload->getUsername())
            ->setUser($user);

        $this->userRepository->save($user);
        $this->profileRepository->save($profile, flush: true);

        return $this->returnJson(
            status: Response::HTTP_CREATED,
            payload: ShowUserResponse::fromUser($user)
        );
    }

    #[Route(path: '/{uuid}', name: '_show', methods: ['GET'])]
    #[IsGranted('user_show', 'user')]
    public function show(User $user, #[CurrentUser] ?User $currentUser): Response
    {
        return $this->returnJson(
            status: Response::HTTP_OK,
            payload: ShowUserResponse::fromUser($user)
        );
    }

    #[Route(path: '/{uuid}', name: '_delete', methods: ['DELETE'])]
    #[IsGranted('user_delete', 'user')]
    public function delete(User $user, #[CurrentUser] ?User $currentUser): Response
    {
        $this->userRepository->remove($user, flush: true);

        return $this->returnJson(
            status: Response::HTTP_NO_CONTENT
        );
    }
}