<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

#[Route(path: '/auth', name: 'public_api_')]
class ApiAuthController extends AbstractController
{
    #[Route(path: '/login', name: 'login', methods: ['POST'])]
    public function login(#[CurrentUser] ?User $user): Response
    {
        if (null === $user) {
            return $this->json([
                'status' => Response::HTTP_UNAUTHORIZED,
                'payload' => [
                    'message' => 'Identifiants invalides',
                ]
            ], Response::HTTP_UNAUTHORIZED);
        }

        return $this->json([
            'status' => Response::HTTP_OK,
            'payload' => [
                'uuid' => $user->getUuid(),
            ]
        ]);
    }

    #[Route(path: '/logout', name: 'logout', methods: ['GET'])]
    public function logout(): never
    {
        throw new \Exception();
    }
}