<?php

declare(strict_types=1);

namespace App\Service;

use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\Exception\NotNormalizableValueException;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AttributeLoader;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class JsonTransformer
{
    private static ?Serializer $serializer = null;

    public static function serializer(): Serializer
    {
        if (null === self::$serializer) {
            self::$serializer = self::createSerializer();
        }

        return self::$serializer;
    }

    /**
     * @param string $json
     * @param class-string<T> $model
     *
     * @return T|null
     */
    public static function fromJSON(string $json, string $model) {
        try {
            /** @var T $deserializedModel */
            $deserializedModel = self::$serializer->deserialize($json, $model, 'json');

            return $deserializedModel;
        } catch (NotNormalizableValueException|NotEncodableValueException) {
            return null;
        }
    }

    private static function createSerializer(): Serializer
    {
        $classMetadataFactory = new ClassMetadataFactory(new AttributeLoader());
        $metadataAwareNameConverter = new MetadataAwareNameConverter($classMetadataFactory);

        return new Serializer(
            normalizers: [
                new ObjectNormalizer(
                    classMetadataFactory: $classMetadataFactory,
                    nameConverter: $metadataAwareNameConverter,
                ),
            ],
            encoders: [
                'json' => new JsonEncoder(),
            ],
        );
    }
}