<?php

declare(strict_types=1);

namespace App\Exception;

use Symfony\Component\Validator\ConstraintViolationListInterface;

class ApiValidationException extends \RuntimeException
{
    public function __construct(
        private readonly ConstraintViolationListInterface $violationList
    ) {
        parent::__construct();
    }

    public function getViolations(): ConstraintViolationListInterface
    {
        return $this->violationList;
    }

    /** @return array<string, string[]> */
    public function getDisplayableViolations(): array
    {
        /** @var array<string, string[]> $errors */
        $errors = [];

        foreach ($this->violationList as $violation) {
            $errors[$violation->getPropertyPath()][] = $violation->getMessage();
        }

        return $errors;
    }
}