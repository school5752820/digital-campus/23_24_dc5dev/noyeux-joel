# Variables
EXEC := "docker compose exec infinite"
COMPOSER := EXEC + " composer"
SYMFONY := EXEC + " symfony"
NPM := EXEC + " npm"

composer +arguments:
  COMPOSER_ALLOW_SUPERUSER=1 {{COMPOSER}} {{arguments}}

console +arguments:
  {{SYMFONY}} console {{arguments}}

npm +arguments:
  {{NPM}} {{arguments}}

migration *arguments:
  {{SYMFONY}} console make:migration {{arguments}}

migrate:
  {{SYMFONY}} console doctrine:migration:migrate --no-interaction

mig: migration migrate

flush:
  {{SYMFONY}} console doctrine:database:drop --force
  {{SYMFONY}} console doctrine:database:create

reset: flush migrate

cc env='dev':
  {{SYMFONY}} console cache:clear --env={{env}}

webpack:
  {{NPM}} run watch

webpack_b:
  {{NPM}} run build

install:
  {{COMPOSER}} install
  {{NPM}} install

test:
  {{EXEC}} php bin/phpunit

new_app:
  {{SYMFONY}} new temp_dir
  {{EXEC}} rm -rf ./temp_dir/.git && cp -R ./temp_dir/. ./ && rm -rf ./temp_dir
