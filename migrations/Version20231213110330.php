<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231213110330 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE "profile_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE "profile" (id INT NOT NULL, created_by_id INT DEFAULT NULL, updated_by_id INT DEFAULT NULL, deleted_by_id INT DEFAULT NULL, username VARCHAR(255) NOT NULL, birthday TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, uuid UUID NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8157AA0FF85E0677 ON "profile" (username)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8157AA0FD17F50A6 ON "profile" (uuid)');
        $this->addSql('CREATE INDEX IDX_8157AA0FB03A8386 ON "profile" (created_by_id)');
        $this->addSql('CREATE INDEX IDX_8157AA0F896DBBDE ON "profile" (updated_by_id)');
        $this->addSql('CREATE INDEX IDX_8157AA0FC76F1F52 ON "profile" (deleted_by_id)');
        $this->addSql('COMMENT ON COLUMN "profile".birthday IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN "profile".uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN "profile".created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN "profile".deleted_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE "profile" ADD CONSTRAINT FK_8157AA0FB03A8386 FOREIGN KEY (created_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "profile" ADD CONSTRAINT FK_8157AA0F896DBBDE FOREIGN KEY (updated_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "profile" ADD CONSTRAINT FK_8157AA0FC76F1F52 FOREIGN KEY (deleted_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "user" ADD profile_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE "user" ADD CONSTRAINT FK_8D93D649CCFA12B8 FOREIGN KEY (profile_id) REFERENCES "profile" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649CCFA12B8 ON "user" (profile_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE "user" DROP CONSTRAINT FK_8D93D649CCFA12B8');
        $this->addSql('DROP SEQUENCE "profile_id_seq" CASCADE');
        $this->addSql('ALTER TABLE "profile" DROP CONSTRAINT FK_8157AA0FB03A8386');
        $this->addSql('ALTER TABLE "profile" DROP CONSTRAINT FK_8157AA0F896DBBDE');
        $this->addSql('ALTER TABLE "profile" DROP CONSTRAINT FK_8157AA0FC76F1F52');
        $this->addSql('DROP TABLE "profile"');
        $this->addSql('DROP INDEX UNIQ_8D93D649CCFA12B8');
        $this->addSql('ALTER TABLE "user" DROP profile_id');
    }
}
