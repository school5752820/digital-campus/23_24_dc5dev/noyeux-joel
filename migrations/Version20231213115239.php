<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231213115239 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE "category_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE "category" (id INT NOT NULL, created_by_id INT DEFAULT NULL, updated_by_id INT DEFAULT NULL, deleted_by_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, uuid UUID NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_64C19C1989D9B62 ON "category" (slug)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_64C19C1D17F50A6 ON "category" (uuid)');
        $this->addSql('CREATE INDEX IDX_64C19C1B03A8386 ON "category" (created_by_id)');
        $this->addSql('CREATE INDEX IDX_64C19C1896DBBDE ON "category" (updated_by_id)');
        $this->addSql('CREATE INDEX IDX_64C19C1C76F1F52 ON "category" (deleted_by_id)');
        $this->addSql('COMMENT ON COLUMN "category".uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN "category".created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN "category".deleted_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE "category" ADD CONSTRAINT FK_64C19C1B03A8386 FOREIGN KEY (created_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "category" ADD CONSTRAINT FK_64C19C1896DBBDE FOREIGN KEY (updated_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "category" ADD CONSTRAINT FK_64C19C1C76F1F52 FOREIGN KEY (deleted_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE page ADD category_id INT NOT NULL');
        $this->addSql('ALTER TABLE page ADD CONSTRAINT FK_140AB62012469DE2 FOREIGN KEY (category_id) REFERENCES "category" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_140AB62012469DE2 ON page (category_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE "page" DROP CONSTRAINT FK_140AB62012469DE2');
        $this->addSql('DROP SEQUENCE "category_id_seq" CASCADE');
        $this->addSql('ALTER TABLE "category" DROP CONSTRAINT FK_64C19C1B03A8386');
        $this->addSql('ALTER TABLE "category" DROP CONSTRAINT FK_64C19C1896DBBDE');
        $this->addSql('ALTER TABLE "category" DROP CONSTRAINT FK_64C19C1C76F1F52');
        $this->addSql('DROP TABLE "category"');
        $this->addSql('DROP INDEX IDX_140AB62012469DE2');
        $this->addSql('ALTER TABLE "page" DROP category_id');
    }
}
