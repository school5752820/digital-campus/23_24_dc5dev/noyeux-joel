<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231213113649 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE "page_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE "page" (id INT NOT NULL, thumbnail_id INT NOT NULL, author_id INT NOT NULL, created_by_id INT DEFAULT NULL, updated_by_id INT DEFAULT NULL, deleted_by_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, markdown VARCHAR(255) NOT NULL, html VARCHAR(255) NOT NULL, published_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, status VARCHAR(255) NOT NULL, uuid UUID NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_140AB620989D9B62 ON "page" (slug)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_140AB620D17F50A6 ON "page" (uuid)');
        $this->addSql('CREATE INDEX IDX_140AB620FDFF2E92 ON "page" (thumbnail_id)');
        $this->addSql('CREATE INDEX IDX_140AB620F675F31B ON "page" (author_id)');
        $this->addSql('CREATE INDEX IDX_140AB620B03A8386 ON "page" (created_by_id)');
        $this->addSql('CREATE INDEX IDX_140AB620896DBBDE ON "page" (updated_by_id)');
        $this->addSql('CREATE INDEX IDX_140AB620C76F1F52 ON "page" (deleted_by_id)');
        $this->addSql('COMMENT ON COLUMN "page".published_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN "page".uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN "page".created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN "page".deleted_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE "page" ADD CONSTRAINT FK_140AB620FDFF2E92 FOREIGN KEY (thumbnail_id) REFERENCES "attachment" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "page" ADD CONSTRAINT FK_140AB620F675F31B FOREIGN KEY (author_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "page" ADD CONSTRAINT FK_140AB620B03A8386 FOREIGN KEY (created_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "page" ADD CONSTRAINT FK_140AB620896DBBDE FOREIGN KEY (updated_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "page" ADD CONSTRAINT FK_140AB620C76F1F52 FOREIGN KEY (deleted_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE "page_id_seq" CASCADE');
        $this->addSql('ALTER TABLE "page" DROP CONSTRAINT FK_140AB620FDFF2E92');
        $this->addSql('ALTER TABLE "page" DROP CONSTRAINT FK_140AB620F675F31B');
        $this->addSql('ALTER TABLE "page" DROP CONSTRAINT FK_140AB620B03A8386');
        $this->addSql('ALTER TABLE "page" DROP CONSTRAINT FK_140AB620896DBBDE');
        $this->addSql('ALTER TABLE "page" DROP CONSTRAINT FK_140AB620C76F1F52');
        $this->addSql('DROP TABLE "page"');
    }
}
