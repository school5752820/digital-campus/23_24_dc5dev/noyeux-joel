<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231213111339 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE "attachment_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE "attachment" (id INT NOT NULL, created_by_id INT DEFAULT NULL, updated_by_id INT DEFAULT NULL, deleted_by_id INT DEFAULT NULL, alt VARCHAR(255) NOT NULL, filename VARCHAR(255) NOT NULL, filepath VARCHAR(255) NOT NULL, filesize INT NOT NULL, mime_type VARCHAR(255) NOT NULL, uuid UUID NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_795FD9BBD17F50A6 ON "attachment" (uuid)');
        $this->addSql('CREATE INDEX IDX_795FD9BBB03A8386 ON "attachment" (created_by_id)');
        $this->addSql('CREATE INDEX IDX_795FD9BB896DBBDE ON "attachment" (updated_by_id)');
        $this->addSql('CREATE INDEX IDX_795FD9BBC76F1F52 ON "attachment" (deleted_by_id)');
        $this->addSql('COMMENT ON COLUMN "attachment".uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN "attachment".created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN "attachment".deleted_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE "attachment" ADD CONSTRAINT FK_795FD9BBB03A8386 FOREIGN KEY (created_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "attachment" ADD CONSTRAINT FK_795FD9BB896DBBDE FOREIGN KEY (updated_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "attachment" ADD CONSTRAINT FK_795FD9BBC76F1F52 FOREIGN KEY (deleted_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE "attachment_id_seq" CASCADE');
        $this->addSql('ALTER TABLE "attachment" DROP CONSTRAINT FK_795FD9BBB03A8386');
        $this->addSql('ALTER TABLE "attachment" DROP CONSTRAINT FK_795FD9BB896DBBDE');
        $this->addSql('ALTER TABLE "attachment" DROP CONSTRAINT FK_795FD9BBC76F1F52');
        $this->addSql('DROP TABLE "attachment"');
    }
}
